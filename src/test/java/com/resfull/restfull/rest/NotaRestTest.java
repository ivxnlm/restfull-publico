package com.resfull.restfull.rest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import com.resfull.restfull.notas.dto.response.ResponseNotaDTO;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class NotaRestTest {
	
	private static final String URL_PATH_NOTAS = "/v1/notas/";

	/**
	 * Puerto local.
	 */
	@LocalServerPort
	private int localPort;

	/**
	 * RestTempleate de tipo test
	 */
	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public final void sendGreetingRest() {

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

		ResponseEntity<ResponseNotaDTO> response = this.restTemplate.exchange(URL_PATH_NOTAS, HttpMethod.GET,
				new HttpEntity<>(headers), ResponseNotaDTO.class);

		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertTrue(response.hasBody());

	}


}
