package com.resfull.restfull.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.controller.NotaController;
import com.resfull.restfull.notas.dto.response.ResponseNotaDTO;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NotaControllerTest {
	
	
	@Autowired
	private NotaController notaController;
	
	/**
	 * Los asserts siempre son necesarios para validar la información que nos
	 * regresa y así poder evaluar el resultado del método que estamos probando
	 */
	/**
	 * En caso de esperar una excepción la cachamos de esta forma. Recuerden nunca
	 * poner un try - catch en una prueba ya que puede haber falsos positivos.
	 */
	//@Test(expected = NotaException.class)
	@Test
	public final void sayHelloTest() {
		ResponseNotaDTO salida = notaController.obtenerNotas();
		Assert.assertTrue("OK".equals(salida.getMessage()));
	}
}
