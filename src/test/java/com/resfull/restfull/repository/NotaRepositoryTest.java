package com.resfull.restfull.repository;

import java.util.Optional;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.resfull.restfull.notas.entity.Nota;
import com.resfull.restfull.notas.repository.NotaRepository;



@RunWith(SpringRunner.class)
@Transactional
@DataJpaTest
//Con esto puedes cargar un esquema desde sql 
@SqlGroup(value = { 
//@Sql(scripts = "/script/schema.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD),
@Sql(scripts = "/script/data.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD) })
@ActiveProfiles("test")
public class NotaRepositoryTest {
	
	@Autowired
	private NotaRepository notaRepository;
	
	@Autowired
	private EntityManager entityManager;
	
	private Nota nota;
	private Nota nota2;
	
	@Before
	public final void initPerson() {
		nota = new Nota(1, "nombre", "titulo", "contenido");
		nota2 = new Nota(2, "nombre2", "titulo2", "contenido2");
	}
	
	@Test
	public final void saveNota() {
		// Almacena a la persona
		notaRepository.save(nota2);
		// Recupera a la persona
		Nota res = notaRepository.findById(2); 
		System.out.println("<<< "+res.getTitulo()+" >>>");
		// Valida el registro
		Assert.assertEquals(2, res.getId());
		entityManager.flush();
	}
	
	@Test
	public final void updateNota() {

		//notaRepository.save(nota2);
		Nota record = notaRepository.findById(1);

		// Valida la informacion Inicial
		System.out.println("informacionActual: "+record.getNombre());
		Assert.assertEquals(1, record.getId());

		// Actualiza a la persona
		Nota updatedNota = new Nota(3, "DatoAct", "DatoAct", "DatoAct");
		record.setNombre("nombreActualizado");
		notaRepository.save(record);

		Nota recordFinal = notaRepository.findById(1);
		
		Assert.assertEquals("nombreActualizado", recordFinal.getNombre());
		
		// Recupera a la persona que se inserta en la bd en el script inicial
		/*Nota updatedRecord = notaRepository.findById(1);

		// Valida el registro
		Assert.assertEquals(EXISTING_PERSON_ID, updatedRecord.get().getId());
		Assert.assertEquals(NAME, updatedRecord.get().getName().trim());
		Assert.assertEquals(AGE, updatedRecord.get().getAge());*/

		entityManager.flush();

	}
	
	@Test
	public final void deletePerson() {

		Nota record = notaRepository.findById(1);

		notaRepository.delete(record);
		
		Nota record2 = notaRepository.findById(1);
		
		
		if(record2==null) {
			Assert.assertTrue(1==1);
		}
		

		entityManager.flush();

	}

	
	
}
