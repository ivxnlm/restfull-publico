package com.resfull.restfull.correo.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.resfull.restfull.alumno.entity.Alumno;

@Entity
@Table(name="correo")
public class Correo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5217946345862397907L;
	
	//esta anotacion hace que si no esta creada la secuencia, la crea en la bd y se pueda usar en natives queries
	//sin considerar este dato
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="id_correo")
	private long id;
	
	@Column(name="correo")
	private String correo;
	
	//Crea la relacion entre muchos muchos correo one alumno
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="id_alumno",foreignKey=@ForeignKey(name="fk_id_correo"),nullable = false, insertable = false, updatable = false) 
	private Alumno alumno;


	
	public Correo() {
		
	}
	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	
	
	
	

}
