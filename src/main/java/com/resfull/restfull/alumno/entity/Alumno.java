package com.resfull.restfull.alumno.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.ForeignKey;

import com.resfull.restfull.correo.entity.Correo;

@Entity
@Table(name="alumno")
public class Alumno implements Serializable{

	private static final long serialVersionUID = 4901453241195386474L;

	//esta anotacion hace que si no esta creada la secuencia, la crea en la bd y se pueda usar en natives queries
	//sin considerar este dato  cambiuo productivo
	//Se crea el autoincrement
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="id_alumno")
	private long id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="apellido")
	private String apellido;
	
	@OneToMany(mappedBy = "alumno")
	private List<Correo> correos;
	

	public Alumno() {
		
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public List<Correo> getCorreos() {
		return correos;
	}
	public void setCorreos(List<Correo> correos) {
		this.correos = correos;
	}

}
