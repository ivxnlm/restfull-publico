package com.resfull.restfull.utils;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

import com.resfull.restfull.notas.dto.response.NotaDTO;
import com.resfull.restfull.notas.entity.Nota;

@Component("convertidor")
public class Convertidor {
	
	public List<NotaDTO> convertirLista(List<Nota> notas){
		List<NotaDTO> notasDto = new ArrayList<>();
		for(Nota nota : notas) {
			notasDto.add(new NotaDTO(nota));
		}
		return notasDto;
	}
	
}
