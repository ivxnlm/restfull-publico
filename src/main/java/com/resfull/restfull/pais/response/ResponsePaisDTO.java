package com.resfull.restfull.pais.response;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

//indica que solo no nulos seran incluidos en el response
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "error",
    "message",
    "contador",
    "paises",
    "pais"
})
@ApiModel(value = "PaisResponse")
public class ResponsePaisDTO {
	
	@ApiModelProperty(required = true, value = "Status should be present")
	@NotNull
	private Integer status;
	
	@ApiModelProperty(required = false, value = "Error may be present")
	@Nullable
	private String error;
	
	@ApiModelProperty(required = true, value = "Message should be present")
	@NotNull
	private String message;
	
	@ApiModelProperty(required = false, value = "Paises may be present")
	@Nullable
	private List<PaisDTO> paises;
	
	@ApiModelProperty(required = false, value = "Contador may be present")
	@Nullable
	private Integer contador;
	
	@ApiModelProperty(required = false, value = "Pais may be present")
	@Nullable
	private PaisDTO pais;
	
	
	public ResponsePaisDTO() {
	}

	

	public ResponsePaisDTO(@NotNull Integer status, String error, @NotNull String message, List<PaisDTO> paises,
			Integer contador, PaisDTO pais) {
		this.status = status;
		this.error = error;
		this.message = message;
		this.paises = paises;
		this.contador = contador;
		this.pais = pais;
	}



	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getContador() {
		return contador;
	}

	public void setContador(Integer contador) {
		this.contador = contador;
	}

	public List<PaisDTO> getPaises() {
		return paises;
	}

	public void setPaises(List<PaisDTO> paises) {
		this.paises = paises;
	}

	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}

}
