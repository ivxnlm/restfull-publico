package com.resfull.restfull.pais.response;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

//indica que solo no nulos seran incluidos en el response
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"nombre",
"continente"
})
@ApiModel(value = "Pais")
public class PaisDTO {
	
	@ApiModelProperty(required = true, value = "Id should be present")
	@NotNull
	private Integer id;
	
	@ApiModelProperty(required = false, value = "Nombre may be present")
	@Nullable
	private String nombre;
	
	@ApiModelProperty(required = false, value = "Continente may be present")
	@Nullable
	private String continente;
	
	public PaisDTO(@NotNull Integer id, String nombre, String continente) {
		this.id = id;
		this.nombre = nombre;
		this.continente = continente;
	}
	
	public PaisDTO() {
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContinente() {
		return continente;
	}

	public void setContinente(String continente) {
		this.continente = continente;
	}
	
}
