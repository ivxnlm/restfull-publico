package com.resfull.restfull.pais.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pais")
public class Pais implements Serializable{

	/**
	 * Este cambio se sube a master
	 * 
	 * 
	 * Se sube un fix urgente
	 */
	private static final long serialVersionUID = 4846526005747990480L;
	
	//esta anotacion hace que si no esta creada la secuencia, la crea en la bd y se pueda usar en natives queries
	//sin considerar este dato
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="id_pais")
	private long id;
	
	@Column(name="nombre_pais")
	private String nombre;
	
	@Column(name="continente")
	private String continente;

	
	public Pais() {
	}

	public Pais(long id, String nombre, String continente) {
		this.id = id;
		this.nombre = nombre;
		this.continente = continente;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContinente() {
		return continente;
	}

	public void setContinente(String continente) {
		this.continente = continente;
	}

	@Override
	public String toString() {
		return "Pais [id=" + id + ", nombre=" + nombre + ", continente=" + continente + "]";
	}
	
}
