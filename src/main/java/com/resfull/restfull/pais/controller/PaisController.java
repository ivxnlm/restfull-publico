package com.resfull.restfull.pais.controller;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.dto.request.ActualizarNotaRequestDTO;
import com.resfull.restfull.notas.dto.request.AgregarNotaRequestDTO;
import com.resfull.restfull.notas.dto.response.NotaDTO;
import com.resfull.restfull.notas.dto.response.ResponseNotaDTO;
import com.resfull.restfull.notas.entity.Nota;
import com.resfull.restfull.pais.entity.Pais;
import com.resfull.restfull.pais.request.ActualizarPaisRequestDTO;
import com.resfull.restfull.pais.request.AgregarPaisRequestDTO;
import com.resfull.restfull.pais.response.PaisDTO;
import com.resfull.restfull.pais.response.ResponsePaisDTO;
import com.resfull.restfull.pais.service.PaisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/*
 * Implementacion de Swagger disponible en 
 * http://localhost:8090/swagger-ui.html
 */
@RestController
@RequestMapping("/v1")
//Crossorigin para que responda desde el fetch
@CrossOrigin
@Api(value = "Nota's microservice", description = "This API has a CRUD for Countries DAO")
public class PaisController {
	
	private static final Log logger = LogFactory.getLog(PaisController.class);
	
	@Value("${id.version}")
	private String idVersion;
	
	@Autowired
	@Qualifier("paisService")
	PaisService paisService;

	/* Obtiene la lista de paises pero usando queries nativos con un Dao
	 * GET
	 * Request
	 * http://localhost:8090/v1/notasNative
	 */
	@GetMapping(value = "/paises",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Countries", notes = "Return a country's List usando DAO" )
	public @ResponseBody ResponsePaisDTO getPaises() throws NotaException{
		logger.info("Version"+idVersion);
		return paisService.obtenerPaises();
	}
	
	/* Obtiene una pais por Id Recibiendo un parametro en el get usando Dao y nativeQuery
	 * GET
	 * Request
	 * http://localhost:8090/v1/nota?id=1
	 */
	
	@GetMapping(value = "/pais",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Country by Id", notes = "Return a country" )
	public @ResponseBody ResponsePaisDTO getPaisById(@RequestParam(value = "id", required = true) Integer idPais ) throws NotaException{
		logger.info("Version + idNota: "+idVersion+" "+idPais);
		return paisService.obtenerPaisById(idPais);
	}
	
	/*
	 * PUT
	 * Request: Agregar Pais
	 * http://localhost:8090/v1/notaNative
	 * Nombre obligatorio ***
	 {
		"nombre":"Nombre Nota 3",
	  	"titulo":"Titulo de nota",
	  	"contenido":"Descripcion de nota"
	 }
	 */
	@PutMapping(value = "/pais",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Add a  Country", notes = "Add a  Country" )
	public @ResponseBody ResponsePaisDTO putPais(@RequestBody @Valid AgregarPaisRequestDTO req) throws NotaException{
		PaisDTO paisDTO = new PaisDTO();
		paisDTO.setNombre(req.getNombre());
		paisDTO.setContinente(req.getContinente());
		return paisService.crearPais(paisDTO); 
	}
	
	/*
	 * POST
	 * Request Actualizar Nota
	 * http://localhost:8090/v1/nota
	 {
	    "id":1,
		"nombre":"Nombre Nota 3",
	  	"titulo":"Titulo de nota",
	  	"contenido":"Descripcion de nota"
	}
	 */
	@PostMapping(value = "pais",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update a Country", notes = "Update a country" )
	public @ResponseBody ResponsePaisDTO postPais(@RequestBody @Valid ActualizarPaisRequestDTO req) throws NotaException{
		PaisDTO pais = new PaisDTO();
		pais.setId(req.getId().intValue());
		pais.setNombre(req.getPais());
		pais.setContinente(req.getContinente());
		return paisService.actualizarPais(pais);
	}
	
	/*
	 * DELETE
	 * Request: Eliminar Pais Se utiliza la forma de parametros en URL ejemplo
	 * http://localhost:8090/v1/nota/5/nota5
	 */
	@DeleteMapping(value = "/pais/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Delete a Country", notes = "Delete a country" )
	public ResponsePaisDTO deletePais(@PathVariable("id") long id) throws NotaException {
		ResponsePaisDTO salida = paisService.eliminarPais(id);
		return salida;
	}
	
	
}
