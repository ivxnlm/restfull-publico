package com.resfull.restfull.pais.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.dto.response.NotaDTO;
import com.resfull.restfull.notas.dto.response.ResponseNotaDTO;
import com.resfull.restfull.notas.entity.Nota;
import com.resfull.restfull.pais.dao.PaisDao;
import com.resfull.restfull.pais.entity.Pais;
import com.resfull.restfull.pais.response.PaisDTO;
import com.resfull.restfull.pais.response.ResponsePaisDTO;

@Service("paisService")
public class PaisService {
	
	private static final Log logger = LogFactory.getLog(PaisService.class);
	
	@Autowired
	@Qualifier("paisDao")
	private PaisDao paisDao;
	
	//Obtiene notas por un Dao es otra alternativa al metodo de arriba obtener las notas
	public ResponsePaisDTO obtenerPaises() throws NotaException{
		logger.info("OBTENIENDO Paises");
		ResponsePaisDTO response  = new ResponsePaisDTO();
		try {
			List<PaisDTO> paises = paisDao.findAll();
			response.setStatus(200);
			response.setMessage("OK");
			response.setPaises(paises);
			response.setContador(paises.size());
		}catch (Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido obtener lo(s) pais(es)");
		}
		return response;
	}
	
	//Obtiene pais por id un Dao es otra alternativa al metodo de arriba obtener las notas
	public ResponsePaisDTO obtenerPaisById(Integer idPais) throws NotaException{
		logger.info("OBTENIENDO PAIS POR ID");
		ResponsePaisDTO response  = new ResponsePaisDTO();
		try {
			PaisDTO pais = paisDao.findById(idPais);
			response.setStatus(200);
			response.setMessage("OK");
			response.setPais(pais);
		}catch (Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido obtener el(s) pais(s)");
		}
		return response;
	}
	
	//Crea un nuevo pais
	public ResponsePaisDTO crearPais(PaisDTO pais) throws NotaException{
		logger.info("CREANDO PAIS");
		ResponsePaisDTO salida = new ResponsePaisDTO();
		try {
			paisDao.save(pais); 
			logger.info("PAIS CREADO");
			salida.setStatus(200);
			salida.setMessage("Pais creado");
		}catch(Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido guardar el pais");
		}
		return salida;
	}
	
	//Elimina un pais
	public ResponsePaisDTO eliminarPais(long idPais) throws NotaException{
		logger.info("ELIMINANDO PAIS");
		ResponsePaisDTO salida = new ResponsePaisDTO();
		try {
			paisDao.delete(idPais);
			logger.info("PAIS ELIMINADO");
			salida.setStatus(200);
			salida.setMessage("Pais eliminado");
		}catch(Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido eliminar el pais");
		}
		return salida;
	}
	
	//actualiza un pais
	public ResponsePaisDTO actualizarPais(PaisDTO pais) throws NotaException{
		logger.info("ACTUALIZANDO PAIS");
		ResponsePaisDTO salida = new ResponsePaisDTO();
		try {
			paisDao.update(pais);
			salida.setStatus(200);
			salida.setMessage("Pais actualizado");
		}catch(Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido actualizar el pais");
		}
		return salida;
	}

}
