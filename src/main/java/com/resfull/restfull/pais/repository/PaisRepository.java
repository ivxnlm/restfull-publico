package com.resfull.restfull.pais.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.resfull.restfull.pais.entity.Pais;

@Repository
public interface PaisRepository extends JpaRepository<Pais,Serializable>{
	
	//public abstract Iterable<List<Pais>> findAll();
	//Estos metodos no es necesario implementarlos
	@Override
	public abstract List<Pais> findAll(); 
	
	public abstract Optional<Pais> findById(Integer id);
	
	
}
