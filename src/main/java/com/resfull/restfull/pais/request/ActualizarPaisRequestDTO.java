package com.resfull.restfull.pais.request;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
  "id",
  "pais",
  "continente"
})
@ApiModel(value = "PaisActualizarRequest")
public class ActualizarPaisRequestDTO {
	
	@ApiModelProperty(required = true, value = "Id should be present")
	@NotNull
	private Long id;
	
	@ApiModelProperty(required = true, value = "Pais should be present")
	@NotNull
	private String pais;
	
	@ApiModelProperty(required = true, value = "Continente should be present")
	@NotNull
	private String continente;
		
	public ActualizarPaisRequestDTO(@NotNull Long id, @NotNull String pais, @NotNull String continente) {
		this.id = id;
		this.pais = pais;
		this.continente = continente;
	}

	public ActualizarPaisRequestDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getContinente() {
		return continente;
	}

	public void setContinente(String continente) {
		this.continente = continente;
	}

	


	

}
