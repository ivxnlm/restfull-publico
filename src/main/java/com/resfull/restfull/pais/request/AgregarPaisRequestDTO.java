package com.resfull.restfull.pais.request;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
  "nombre",
  "continente"
})
@ApiModel(value = "PaisAgregarRequest")
public class AgregarPaisRequestDTO {
	
	@ApiModelProperty(required = true, value = "Nombre should be present")
	@NotNull
	private String nombre;
	
	@ApiModelProperty(required = true, value = "Continente should be present")
	@NotNull
	private String continente;

	public AgregarPaisRequestDTO(@NotNull String nombre, @NotNull String continente) {
		this.nombre = nombre;
		this.continente = continente;
	}

	public AgregarPaisRequestDTO() {
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContinente() {
		return continente;
	}

	public void setContinente(String continente) {
		this.continente = continente;
	}
	
	

}
