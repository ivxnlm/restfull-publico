package com.resfull.restfull.pais.dao;

import java.util.List;

import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.pais.response.PaisDTO;

public interface PaisDao {

	public List<PaisDTO> findAll() throws NotaException;
	
	public PaisDTO findById (Integer idPais) throws NotaException;
	
	public boolean save (PaisDTO paisDTO) throws NotaException;
	
	public boolean delete (Long idPais) throws NotaException;
	
	public boolean update (PaisDTO paisDTO) throws NotaException;
	
}

