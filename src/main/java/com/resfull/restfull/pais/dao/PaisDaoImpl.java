package com.resfull.restfull.pais.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.dto.response.NotaDTO;
import com.resfull.restfull.pais.response.PaisDTO;

@Repository("paisDao")
public class PaisDaoImpl implements PaisDao{

	private static final Log logger = LogFactory.getLog(PaisDaoImpl.class);
	
	@Autowired
	private EntityManager entityManager;
	
	
	@Override
	public List<PaisDTO> findAll() throws NotaException {
		List<PaisDTO> salida = new ArrayList<>();
		try {
			//este es el orden que lleva el array del select
			String cadena = "select ID_PAIS,NOMBRE_PAIS,CONTINENTE from PAIS";
			Query nativeQuery = entityManager.createNativeQuery(cadena);
			List<Object[]> res = nativeQuery.getResultList();
			salida = res.stream().map(index -> new PaisDTO(((BigInteger)index[0]).intValue(), (String)index[1], (String)index[2]))
						.collect(Collectors.toList());
		}catch(Exception e) {
			logger.info(e.getMessage());
		}
		return salida;
	}

	@Override
	public PaisDTO findById(Integer idPais) throws NotaException {
		
		PaisDTO salida = new PaisDTO();
		try {
			String cadena = "select ID_PAIS,NOMBRE_PAIS,CONTINENTE from PAIS where ID_PAIS = ?";
			Query nativeQuery = entityManager.createNativeQuery(cadena);
			nativeQuery.setParameter(1, idPais);
			Object[] res = (Object[])nativeQuery.getSingleResult(); 
			salida.setId(((BigInteger) res[0]).intValue());
			salida.setNombre((String) res[1]);
			salida.setContinente((String) res[2]);
		}catch(Exception e) {
			logger.info(e.getMessage());
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido obtener el pais");
		}
		return salida;
	}


	//Etiqueta transactional para insert, update, delete
	@Transactional
	@Override
	public boolean save(PaisDTO paisDTO) throws NotaException {
		boolean salida = false;
		try {
			Query query = entityManager.createNativeQuery("INSERT INTO PAIS (CONTINENTE, NOMBRE_PAIS) VALUES(:continente,:pais)");
	        query.setParameter("continente", paisDTO.getContinente());
	        query.setParameter("pais", paisDTO.getNombre());
	        query.executeUpdate();
	        salida = true;
		}catch(Exception e) {
			salida = false;
			logger.info(e.getMessage());
			throw new NotaException("No se ha podido guardar el pais");
		}
		return salida;
	}

	//Etiqueta transactional para insert, update, delete
	@Transactional
	@Override
	public boolean delete(Long idPais) throws NotaException {
		// TODO Auto-generated method stub
		boolean salida = false;
		try {
			Query query = entityManager.createNativeQuery("DELETE FROM PAIS WHERE ID_PAIS= ? ");
	        query.setParameter(1, idPais);
	        int x = query.executeUpdate();
	        if(x==0)
	        	throw new NotaException("No se ha podido eliminar el pais");
		}catch(Exception e) {
			salida = false;
			logger.info(e.getMessage());
			throw new NotaException("No se ha podido eliminar el pais");
		}
		return salida;
	}

	//Etiqueta transactional para insert, update, delete
	@Transactional
	@Override
	public boolean update(PaisDTO paisDTO) throws NotaException {
		boolean salida = false;
		try {
			Query query = entityManager.createNativeQuery("UPDATE PAIS SET CONTINENTE = ?, NOMBRE_PAIS = ? WHERE ID_PAIS= ? ");
			query.setParameter(1, paisDTO.getNombre());
			query.setParameter(2, paisDTO.getNombre());
			query.setParameter(3, paisDTO.getId());
	        int x = query.executeUpdate();
	        if(x==0)
	        	throw new NotaException();
		}catch(Exception e) {
			salida = false;
			logger.info(e.getMessage());
			throw new NotaException("No se ha podido actualizar el pais");
		}
		return salida;
	}

}
