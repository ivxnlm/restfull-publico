package com.resfull.restfull.controller;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.controller.NotaController;
import com.resfull.restfull.notas.dto.response.ResponseNotaDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/*
 * Implementacion de Swagger disponible en 
 * http://localhost:8090/swagger-ui.html
 */
@RestController
@RequestMapping("")
//Crossorigin para que responda desde el fetch
@CrossOrigin
@Api(value = "Nota's microservice", description = "This API has a CRUD for users REPOSITORY")
public class DemoController {
	
	
	private static final Log logger = LogFactory.getLog(DemoController.class);
	
	
	/*
	 * GET
	 * Request
	 * http://localhost:8090/demoGet
	 */
	@GetMapping(value = "/demoGet",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Demo", notes = "Return a note's List" )
	public @ResponseBody HashMap<String, Object> demoGet() throws NotaException{
		logger.info("Version");
		
		HashMap<String, Object>  response = new HashMap<>();
		
		response.put("estatus", 200);
		
		return response;
	}
	
	/*
	 * Post
	 * Request {}
	 * http://localhost:8090/demoPost
	 */
	@PostMapping(value = "/demoPost",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Demo", notes = "Return a note's List" )
	public @ResponseBody HashMap<String, Object> demoPost(HashMap<String, Object> request) throws NotaException{
		logger.info("Version");
		
		HashMap<String, Object>  response = new HashMap<>();
		
		response.put("estatus", 200);
		
		return response;
	}

}
