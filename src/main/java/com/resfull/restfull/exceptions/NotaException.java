package com.resfull.restfull.exceptions;

public class NotaException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public NotaException() {
		super();
	}

	public NotaException(String message) {
		super(message);
	}

	public NotaException(String message, Throwable cause) {
        super(message, cause);
    }

	public NotaException(Throwable cause) {
		super(cause);
	}

}
