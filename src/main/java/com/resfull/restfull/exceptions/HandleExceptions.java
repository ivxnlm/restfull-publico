package com.resfull.restfull.exceptions;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class HandleExceptions extends ResponseEntityExceptionHandler{
	
    //400 Bad request: conversion types
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.BAD_REQUEST.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Malformed request");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST,
                request);
    }
    
    //400 Bad request by spring validator
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    	logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.BAD_REQUEST.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Malformed request");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST,
                request);
    }
    
    //400 Bad request by spring BindException
    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
    		WebRequest request) {
    	logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.BAD_REQUEST.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Malformed request");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST,
                request);
    }
    
    //400 Bad Request conversion types
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.BAD_REQUEST.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Malformed request mismatch");//----------------------
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST,
                request);
    }

    //400
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(final MissingServletRequestPartException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    	logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.BAD_REQUEST.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Malformed request");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST,
                request);
    }

    //400 Malformed request: getparamname 
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(final MissingServletRequestParameterException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    	logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.BAD_REQUEST.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Malformed request");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST,
                request);
    }
    
    //404 Not found (Needed @EnableWebMvc and context found in @SpringBootApplication for work)
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
    		HttpStatus status, WebRequest request) {
    	logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.NOT_FOUND.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Not found resource");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND,
                request);
    }
    
    //405 Method not supported
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    	logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.METHOD_NOT_ALLOWED.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Method not supported");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED,
                request);
    }
    
    //415 Unsupported media type
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(final HttpMediaTypeNotSupportedException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
    	bodyOfResponse.put("status", HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Media type is not supported");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.UNSUPPORTED_MEDIA_TYPE,
                request);
    }
    
    //Personalized Exception
    @ExceptionHandler({ NotaException.class })
    protected ResponseEntity<Object> configuradorException(final NotaException ex, final WebRequest request) {
    	logger.info(ex.getClass().getName());
    	Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
        bodyOfResponse.put("error", "Nota Error");
        bodyOfResponse.put("message", ex.getMessage());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
    
	//Default 500 
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);
        Map<String, Object> bodyOfResponse = new HashMap<>();
        bodyOfResponse.put("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
        bodyOfResponse.put("error", "General error");
        bodyOfResponse.put("message", "Application error");
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }
	

}
