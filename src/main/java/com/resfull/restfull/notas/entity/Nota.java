package com.resfull.restfull.notas.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="nota")
//@XmlRootElement Esta etiqueta sirve para enviar xml en lugar de json en el request
public class Nota implements Serializable{
	
	private static final long serialVersionUID = 7123367535040319049L;

	
	//esta anotacion hace que si no esta creada la secuencia, la crea en la bd y se pueda usar en natives queries
	//sin considerar este dato
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="id_nota")
	private long id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="titulo")
	private String titulo;
	
	@Column(name="contenido")
	private String contenido;

	public Nota() {
	}

	public Nota(long id, String nombre, String titulo, String contenido) {
		this.id = id;
		this.nombre = nombre;
		this.titulo = titulo;
		this.contenido = contenido;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	@Override
	public String toString() {
		return "MNota [id=" + id + ", nombre=" + nombre + ", titulo=" + titulo + ", contenido=" + contenido + "]";
	}

}

