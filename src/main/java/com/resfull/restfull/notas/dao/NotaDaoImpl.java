package com.resfull.restfull.notas.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.controller.NotaController;
import com.resfull.restfull.notas.dto.response.NotaDTO;

@Repository("notaNativeDao")
public class NotaDaoImpl implements NotaDao{
	
	private static final Log logger = LogFactory.getLog(NotaDaoImpl.class);

	@Autowired
	private EntityManager entityManager;
	
	@Override
	public List<NotaDTO> obtenerNotasDao() throws NotaException{
		List<NotaDTO> salida = new ArrayList<>();
		try {
			//este es el orden que lleva el array del select
			String cadena = "select ID_NOTA,NOMBRE,TITULO,CONTENIDO from NOTA";
			Query nativeQuery = entityManager.createNativeQuery(cadena);
			List<Object[]> res = nativeQuery.getResultList(); 
			salida = res.stream().map(index -> new NotaDTO(((BigInteger)index[0]).longValue(), (String)index[1], (String)index[2], (String)index[3]))
						.collect(Collectors.toList());
		}catch(Exception e) {
			logger.info(e.getMessage());
		}
		return salida;
	}



	@Override
	public NotaDTO obtenerNotaPorIdDao(Integer idNota) throws NotaException {
		NotaDTO salida = new NotaDTO();
		try {
			String cadena = "select ID_NOTA,NOMBRE,TITULO,CONTENIDO from NOTA where ID_NOTA = ?";
			Query nativeQuery = entityManager.createNativeQuery(cadena);
			nativeQuery.setParameter(1, idNota);
			Object[] res = (Object[])nativeQuery.getSingleResult(); 
			salida.setId(((BigInteger) res[0]).longValue());
			salida.setNombre((String) res[1]);
			salida.setTitulo((String) res[2]);
			salida.setContenido((String) res[3]);
		}catch(Exception e) {
			logger.info(e.getMessage());
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido obtener la nota");
		}
		return salida;
	}



	//Etiqueta transactional para insert, update, delete
	@Transactional
	@Override
	public boolean guardarNota(NotaDTO notaDTO) throws NotaException {
		try {
			Query query = entityManager.createNativeQuery("INSERT INTO NOTA (NOMBRE, TITULO, CONTENIDO) VALUES(:nombre,:titulo,:contenido)");
	        query.setParameter("nombre", notaDTO.getNombre());
	        query.setParameter("titulo", notaDTO.getTitulo());
	        query.setParameter("contenido", notaDTO.getContenido());
	        query.executeUpdate();
		}catch(Exception e) {
			logger.info(e.getMessage());
		}
		return false;
	}

}
