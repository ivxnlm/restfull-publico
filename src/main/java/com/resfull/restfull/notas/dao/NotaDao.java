package com.resfull.restfull.notas.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.dto.response.NotaDTO;


public interface NotaDao {
	
	public List<NotaDTO> obtenerNotasDao () throws NotaException;
	
	public NotaDTO obtenerNotaPorIdDao (Integer idNota) throws NotaException;
	
	public boolean guardarNota (NotaDTO notaDTO) throws NotaException;
	

}
