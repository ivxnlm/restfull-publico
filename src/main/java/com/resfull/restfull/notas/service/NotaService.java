package com.resfull.restfull.notas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.dao.NotaDao;
import com.resfull.restfull.notas.dto.response.NotaDTO;
import com.resfull.restfull.notas.dto.response.ResponseNotaDTO;
import com.resfull.restfull.notas.entity.Nota;
import com.resfull.restfull.notas.repository.NotaRepository;
import com.resfull.restfull.utils.Convertidor;

import ch.qos.logback.classic.Logger;

@Service("notaService")
public class NotaService {
	
	private static final Log logger = LogFactory.getLog(NotaService.class);
	
	@Autowired
	@Qualifier("notaRepository")
	private NotaRepository notaRepository;
	
	@Autowired
	@Qualifier("convertidor")
	private Convertidor convertidor;
	
	/**
	 * Servicio que crea una nota
	 * @param nota
	 * @return
	 * @throws NotaException
	 */
	public ResponseNotaDTO crearNota(Nota nota) throws NotaException{
		logger.info("CREANDO NOTA");
		ResponseNotaDTO salida = new ResponseNotaDTO();
		try {
			notaRepository.save(nota);
			logger.info("NOTA CREADA");
			salida.setStatus(200);
			salida.setMessage("Nota creada");
		}catch(Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido guardar la nota");
		}
		
		return salida;
	}
		
	/**
	 * Servicio que actualiza una nota
	 * @param nota
	 * @return
	 * @throws NotaException
	 */
	public ResponseNotaDTO actualizar(Nota nota) throws NotaException{
		logger.info("ACTUALIZANDO NOTA");
		ResponseNotaDTO salida = new ResponseNotaDTO();
		try {
			notaRepository.save(nota);
			salida.setStatus(200);
			salida.setMessage("Nota actualizada");
		}catch(Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido actualizar la nota");
		}
		return salida;
	}
	
	/**
	 * Servicio que elimina una nota
	 * @param id
	 * @return
	 * @throws NotaException
	 */
	public ResponseNotaDTO borrar(long id) throws NotaException{
		logger.info("BORRANDO NOTA");
		ResponseNotaDTO salida = new ResponseNotaDTO();
		try {
			Nota nota = notaRepository.findById(id);
			notaRepository.delete(nota);
			salida.setStatus(200);
			salida.setMessage("Nota Borrada");
		}catch(Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido eliminar la nota");
		}
		return salida;
	}
	
	/**
	 * Servicio que obtiene notas
	 * @return
	 * @throws NotaException
	 */
	public ResponseNotaDTO obtener() throws NotaException{
		logger.info("OBTENIENDO NOTAS");
		ResponseNotaDTO response  = new ResponseNotaDTO();
		try {
			List<Nota> notas = notaRepository.findAll();
			List<NotaDTO> notasDTO = convertidor.convertirLista(notas);
			response.setStatus(200);
			response.setMessage("OK");
			response.setNotas(notasDTO);
			response.setContador(notas.size());
		}catch (Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido obtener la(s) nota(s)");
		}
		return response;
	}
	
	/**
	 * Servicio que obtiene notas by id
	 * @param idNota
	 * @return
	 * @throws NotaException
	 */
	public ResponseNotaDTO obtenerNotaById(Long idNota) throws NotaException{
		logger.info("OBTENIENDO NOTA BY ID");
		ResponseNotaDTO response  = new ResponseNotaDTO();
		try {
			List<Nota> notas = new ArrayList<>();
			Optional<Nota> nota = notaRepository.findById(idNota);
			notas.add(nota.get());
			List<NotaDTO> notasDTO = convertidor.convertirLista(notas);
			response.setStatus(200);
			response.setMessage("OK");
			response.setNota(notasDTO.get(0));
			response.setContador(notas.size());
		}catch (Exception e) {
			logger.error("HUBO UN ERROR");
			throw new NotaException("No se ha podido obtener la(s) nota(s)");
		}
		return response;
	}

	/**
	 * Servicio que obtiene notas paginadas
	 * @param pageable
	 * @return
	 */
	public List<NotaDTO> obtenerPorPaginacion(Pageable pageable){
		List<Nota> notas = notaRepository.findAll(pageable).getContent();
		List<NotaDTO> mNotas = convertidor.convertirLista(notas);
		return mNotas;
	}

}
