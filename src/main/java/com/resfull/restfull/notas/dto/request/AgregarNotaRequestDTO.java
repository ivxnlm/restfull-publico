package com.resfull.restfull.notas.dto.request;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
  "nombre",
  "titulo",
  "contenido"
})
@ApiModel(value = "NotaAgregarRequest")
public class AgregarNotaRequestDTO {
	
	@ApiModelProperty(required = true, value = "Nombre should be present")
	@NotNull
	private String nombre;
	
	@ApiModelProperty(required = false, value="Titulo may be optional")
	@Nullable
	private String titulo;
	
	@ApiModelProperty(required = false, value="Titulo may be optional")
	@Nullable
	private String contenido;
	
	public AgregarNotaRequestDTO(String nombre, String titulo, String contenido) {
		this.nombre = nombre;
		this.titulo = titulo;
		this.contenido = contenido;
	}
	
	public AgregarNotaRequestDTO() {
	}



	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	

}
