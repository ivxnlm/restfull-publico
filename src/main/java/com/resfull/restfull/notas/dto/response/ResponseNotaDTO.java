package com.resfull.restfull.notas.dto.response;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

//indica que solo no nulos seran incluidos en el response
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "error",
    "message",
    "contador",
    "notas",
    "nota"
})
@ApiModel(value = "NotaResponse")
public class ResponseNotaDTO {
	
	@ApiModelProperty(required = true, value = "Status should be present",example = "999")
	@NotNull
	private Integer status;
	
	@ApiModelProperty(required = false, value = "Error may be present")
	@Nullable
	private String error;
	
	@ApiModelProperty(required = true, value = "Message should be present")
	@NotNull
	private String message;
	
	@ApiModelProperty(required = false, value = "Notas may be present")
	@Nullable
	private List<NotaDTO> notas;
	
	@ApiModelProperty(required = false, value = "Contador may be present")
	@Nullable
	private Integer contador;
	
	@ApiModelProperty(required = false, value = "Nota should be present")
	@Nullable
	private NotaDTO nota;
	
	
	public ResponseNotaDTO() {
	}

	public ResponseNotaDTO(@NotNull Integer status, String error, @NotNull String message, List<NotaDTO> notas,
			Integer contador) {
		this.status = status;
		this.error = error;
		this.message = message;
		this.notas = notas;
		this.contador = contador;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<NotaDTO> getNotas() {
		return notas;
	}
	public void setNotas(List<NotaDTO> notas) {
		this.notas = notas;
	}

	public Integer getContador() {
		return contador;
	}

	public void setContador(Integer contador) {
		this.contador = contador;
	}

	public NotaDTO getNota() {
		return nota;
	}

	public void setNota(NotaDTO nota) {
		this.nota = nota;
	}
	
	

}
