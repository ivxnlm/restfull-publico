package com.resfull.restfull.notas.dto.response;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.resfull.restfull.notas.entity.Nota;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

//indica que solo no nulos seran incluidos en el response
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
  "id",
  "nombre",
  "titulo",
  "contenido"
})
@ApiModel(value = "Nota")
public class NotaDTO {
	
	@ApiModelProperty(required = true, value = "Id should be present")
	@NotNull
	private Long id;
	
	@ApiModelProperty(required = false, value = "Nombre may be present")
	@Nullable
	private String nombre;
	
	@ApiModelProperty(required = false, value = "Titulo may be present")
	@Nullable
	private String titulo;
	
	@ApiModelProperty(required = false, value = "Contenido may be present")
	@Nullable
	private String contenido;
	
	public NotaDTO() {
	}
	
	public NotaDTO (Nota nota){
		this.id = nota.getId();
		this.nombre = nota.getNombre();
		this.titulo = nota.getTitulo();
		this.contenido = nota.getContenido();
	}
	
	public NotaDTO(Long id, String nombre, String titulo, String contenido) {
		this.id = id;
		this.nombre = nombre;
		this.titulo = titulo;
		this.contenido = contenido;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

}
