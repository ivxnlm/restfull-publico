package com.resfull.restfull.notas.dto.request;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
  "id",
  "nombre",
  "titulo",
  "contenido"
})
@ApiModel(value = "NotaActualizarRequest")
public class ActualizarNotaRequestDTO {
	
    @ApiModelProperty(required = true, value = "Id should be present")
	@NotNull
	private Integer id;
	
    @ApiModelProperty(required = true, value = "Nombre should be present")
	@NotNull
	private String nombre;
	
    @ApiModelProperty(required = true, value = "Titulo should be present")
	@NotNull
	private String titulo;
	
    @ApiModelProperty(required = true, value = "Contenido should be present")
	@NotNull
	private String contenido;
	
	

	public ActualizarNotaRequestDTO(@NotNull Integer id, @NotNull String nombre, @NotNull String titulo,
			@NotNull String contenido) {
		this.id = id;
		this.nombre = nombre;
		this.titulo = titulo;
		this.contenido = contenido;
	}

	public ActualizarNotaRequestDTO() {
	}



	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	

}
