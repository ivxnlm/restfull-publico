package com.resfull.restfull.notas.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.resfull.restfull.exceptions.NotaException;
import com.resfull.restfull.notas.dto.request.ActualizarNotaRequestDTO;
import com.resfull.restfull.notas.dto.request.AgregarNotaRequestDTO;
import com.resfull.restfull.notas.dto.response.NotaDTO;
import com.resfull.restfull.notas.dto.response.ResponseNotaDTO;
import com.resfull.restfull.notas.entity.Nota;
import com.resfull.restfull.notas.service.NotaService;
import com.resfull.restfull.pais.response.ResponsePaisDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;



/*
 * Implementacion de Swagger disponible en 
 * http://localhost:8090/swagger-ui.html
 */
@RestController
@RequestMapping("/v1")
//Crossorigin para que responda desde el fetch
@CrossOrigin
@Api(value = "Nota's microservice", description = "This API has a CRUD for users REPOSITORY")
public class NotaController {
	
	private static final Log logger = LogFactory.getLog(NotaController.class);

	@Value("${id.version}")
	private String idVersion;
	
	@Autowired
	@Qualifier("notaService")
	NotaService notaService;
	
	/*
	 * GET
	 * Request
	 * http://localhost:8090/v1/notas
	 */
	@GetMapping(value = "/notas",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Notes", notes = "Return a note's List" )
	public @ResponseBody ResponseNotaDTO obtenerNotas() throws NotaException{
		logger.info("Version"+idVersion);
		return notaService.obtener();
	}
	
	/* Obtiene una nota por Id Recibiendo un parametro en el get usando repository
	 * GET
	 * Request
	 * http://localhost:8090/v1/nota?id=1
	 */
	
	@GetMapping(value = "/nota",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Note by Id", notes = "Return a note" )
	public @ResponseBody ResponseNotaDTO getNotaById(
			@RequestParam(value = "id", required = true)
			Long idNota ) throws NotaException{
		logger.info("Version + idNota: "+idVersion+" "+idNota);
		return notaService.obtenerNotaById(idNota); 
	}
	
	
	/*
	 * PUT
	 * Request: Agregar Nota
	 * http://localhost:8090/v1/nota
	 * Nombre obligatorio ***
	 {
		"nombre":"Nombre Nota 3",
	  	"titulo":"Titulo de nota",
	  	"contenido":"Descripcion de nota"
	 }
	 */
	@PutMapping(value = "/nota",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Add a Note", notes = "Add a note" )
	public @ResponseBody ResponseNotaDTO agregarNota(@RequestBody @Valid AgregarNotaRequestDTO req) throws NotaException{
		Nota n = new Nota();
		n.setNombre(req.getNombre());
		n.setTitulo(req.getTitulo());
		n.setContenido(req.getContenido());
		return notaService.crearNota(n);
	}
	
	
	/*
	 * POST
	 * Request Actualizar Nota
	 * http://localhost:8090/v1/nota
	 {
	    "id":1,
		"nombre":"Nombre Nota 3",
	  	"titulo":"Titulo de nota",
	  	"contenido":"Descripcion de nota"
	}
	 */
	@PostMapping(value = "/nota",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Update a Note", notes = "Update a note" )
	public @ResponseBody ResponseNotaDTO actualizarNota(@RequestBody @Valid ActualizarNotaRequestDTO req) throws NotaException{
		Nota n = new Nota();
		n.setId(req.getId());
		n.setNombre(req.getNombre());
		n.setTitulo(req.getTitulo());
		n.setContenido(req.getContenido());
		return notaService.actualizar(n);
	}
	
	/*
	 * DELETE
	 * Request: Eliminar Nota Se utiliza la forma de parametros en URL ejemplo
	 * http://localhost:8090/v1/nota/5/nota5
	 */
	@DeleteMapping(value = "/nota/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Delete a Note", notes = "Delete a note" )
	public ResponseNotaDTO borrarNota(@PathVariable("id") long id) throws NotaException {
		ResponseNotaDTO salida = notaService.borrar(id);
		return salida;
	}
	
	
	/*
	 * GET
	 * Request
	 * http://localhost:8090/v1/notasp?page=0&size=5
	 */
	@GetMapping("/notasp")
	@ApiOperation(value = "Get Notes with pagination", notes = "Get Notes with pagination" )
	public List<NotaDTO> obtenerNotasP(Pageable pageable) {
		return notaService.obtenerPorPaginacion(pageable);
	}
	
}
